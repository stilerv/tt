<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 14.09.2018
 * Time: 16:43
 */

namespace gun_machine\builds;


use gun_machine\classes\controllerClass;

class userController extends controllerClass
{

    public $template = '';

    public function loginAction()
    {
        if (isset($_POST['login'])){
            $user = userModel::getUser(['email'=>trim($_POST['email'])]);
            if (password_verify($_POST['pass'],$user->pass)){
                $_SESSION['user_id'] = $user->id;
                $this->redirect('/user/view');
            }
        }
        $this->partialRender('login_form');
    }

    public function logoutAction()
    {
        unset($_SESSION['user_id']);
        $this->redirect('/');
    }

    public function viewAction()
    {
        $this->template = 'user_view';
        if ($user = userModel::isLogged()){
            $this->render(['user'=>$user]);
        }else{
            $this->redirect('/');
        }
    }

    public function editAction()
    {
        $this->template = 'user_edit';
        if ($user = userModel::isLogged()){
            if (isset($_POST['edit'])){
                $props = [];
                foreach (userModel::editableFields() as $attr){
                    if (isset($_POST[$attr]) && !is_null($_POST[$attr])){
                        $props[$attr] = $_POST[$attr];
                    }
                }
                if (isset($_POST['pass']) && $_POST['pass'] !='' && $_POST['pass'] == $_POST['confirm_pass']){
                    $props['pass'] = password_hash($_POST['pass'],PASSWORD_BCRYPT);
                }
                $user = userModel::updateUser($props,$user->id);
                $_SESSION['user'] = $user;
            }
            $this->render(['user'=>$user]);
        }else{
            $this->redirect('/');
        }
    }

    public function registerAction()
    {
        $tpl = 'register_form';
        $params = [];
        if (isset($_POST['register'])) {
            $email = trim($_POST['email']);
            $pass = $_POST['pass'];
            $confirm = $_POST['confirm_pass'];

            if (empty($email)) {
                $params['email_status'] = 'invalid email';
            }

            if ($pass !== $confirm) {
                $params['pass_status'] = 'password and confirmed password no equal';
            }

            if (!empty($email) && $pass != '' && $confirm != '' && $pass == $confirm) {
                $user = userModel::getUser(['email' => $email]);
                if (!empty($user)) {
                    $params['email_status'] = 'User with same email is registered, change to another email.';
                } else {
                    $props = [
                        'email' => $email,
                        'pass' => password_hash($pass, PASSWORD_BCRYPT),
                    ];
                    if (!empty($_GET['reg_admin']) && $_GET['reg_admin'] = REGISTER_ADMIN_SECRET_KEY){
                        $props['role'] = 1;
                    }
                    $new_user = userModel::newUser($props);
                    if (!empty($new_user)){
                        $tpl = 'register_msg';

                        $subject = 'Your registration';
                        $msg = self::getPartialRender('register_email_msg',['user'=>$new_user]);
                        $headers = "MIME-Version: 1.0" . "\r\n";
                        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                        $headers .= 'From: <tt@example.com>' . "\r\n";
                        mail($new_user->email,$subject,$msg,$headers);
                    }
                }
            }
        }
        self::partialRender($tpl,$params);
    }

    public function activateAction(){
        $token = !empty($this->route->url_params[0]) ? $this->route->url_params[0] : '';
        $user = userModel::getUser(['register_token'=>$token,'status'=>0]);
        if (!empty($user)){
            $user = userModel::updateUser(['status'=>1],$user->id);
            if (!empty($user)){
                $_SESSION['user_id'] = $user->id;
                $this->redirect('/user/view');
            }
        }
        self::partialRender('activate_user_msg');
    }

    public function resetAction()
    {
        $tpl = 'reset';
        $params = [];
        if (isset($_POST['reset'])){
            if (empty($_POST['email'])){
                $params['email_status'] = 'Incorrect email';
            }else{
                $user = userModel::getUser(['email'=>trim($_POST['email'])]);
                if (empty($user)){
                    $params['email_status'] = 'User with same email not found.';
                }else{
                    $tmstmp = time();
                    $props = [
                        'reset_token' => md5('reset_pass'.$tmstmp),
                        'reset_timestamp' => date('Y-m-d H:i:s',$tmstmp)
                    ];
                    $user = userModel::updateUser($props,$user->id);
                    if (!empty($user)){
                        $tpl = 'reset_msg';

                        $subject = 'Password reset';
                        $msg = self::getPartialRender('reset_email_msg',['user'=>$user]);
                        $headers = "MIME-Version: 1.0" . "\r\n";
                        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                        $headers .= 'From: <tt@example.com>' . "\r\n";
                        mail($user->email,$subject,$msg,$headers);
                    }
                }
            }
        }

        if (isset($_POST['submit_reset']) && !empty($_POST['reset_token'])){
            $tpl = 'reset_set_pass';
            $user = userModel::getUser(['reset_token'=>$_POST['reset_token']]);

            if (empty($user)){
                print_r('<div class="alert alert-warning" role="alert">Reset token is not find</div>');
            }else{
                $time= time();
                if ($time - strtotime($user->reset_timestamp) > RESET_PASSWORD_TOKEN_LIVE_TIME){
                    print_r('<div class="alert alert-warning" role="alert">Reset token is past live time</div>');
                }else{
                    if($_POST['pass'] == $_POST['confirm_pass'] && $_POST['pass'] != ''){
                        $props = [
                            'pass' => password_hash($_POST['pass'],PASSWORD_BCRYPT),
                            'reset_token' => '',
                        ];
                        $user = userModel::updateUser($props,$user->id);
                        $this->redirect('/user/login');
                    }else{
                        $params = [
                            'pass_status' => 'incorrect password',
                            'token' => $user->reset_token
                        ];
                    }
                }
            }
        }
        $token = !empty($this->route->url_params[0]) ? $this->route->url_params[0] : '';
        if (!empty($token) && !isset($_POST['reset']) && !isset($_POST['submit_reset'])){
            $tpl = 'reset_set_pass';
            $params = ['token'=>$token];
        }

        self::partialRender($tpl,$params);
    }
}