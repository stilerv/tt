<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 14.09.2018
 * Time: 16:55
 */

namespace gun_machine\builds;


use gun_machine\classes\controllerClass;

class pageController extends controllerClass
{
    public $template = 'main';

    public function mainAction(){
        $this->render(['params'=>$_SERVER]);
    }
}