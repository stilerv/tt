<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 15.09.2018
 * Time: 13:30
 */

namespace gun_machine\builds;

use gun_machine\classes\queryBuilderClass;

/**
 * Class userModel
 * @package gun_machine\builds
 * @property integer $id
 * @property string  $email
 * @property string  $pass
 * @property string  $first_name
 * @property string  $last_name
 * @property string  $register_token
 * @property string  $reset_token
 * @property string  $reset_timestamp
 * @property string  $avatar
 * @property int     $role
 * @property string  $created_at
 * @property string  $updated_at
 * @property int     $status
 */
class userModel
{
    const TABLE = 'user';
    const USER_STATUS_NOT_CONFIRMED = 0;
    const USER_STATUS_CONFIRMED = 1;

    const USER_ROLE_ADMIN = 1;
    const USER_ROLE_USER = 2;

    public function __construct()
    {
    }

    public static function editableFields(){
        return [
            'email',
            'first_name',
            'last_name',
//            'pass'
//            'avatar',
//            'created_at',
//            'updated_at',
        ];
    }

    public static function viewedFields(){
        return [
            'email',
            'first_name',
            'last_name',
//            'avatar',
            'created_at',
            'updated_at',
        ];
    }

    public static function isLogged()
    {
//        if (!empty($_SESSION['user_id'])) {
//            $user = userModel::getUser(['id'=>$_SESSION['user_id']]);
//            $_SESSION['user_id'] = $user->id;
//        }
        return !empty($_SESSION['user']) ? $_SESSION['user'] : false;
    }

    /**
     * @param array $props
     * @return mixed|null|userModel
     */
    public static function getUser($props = [])
    {
        $user = null;
        if (is_numeric($props)) {
            $props = ['id' => intval($props)];
        }
        if (is_string($props)) {
            $props = ['email' => strval($props)];
        }
        if (is_array($props) && !empty($props)) {
            $where = [];
            $prop_values = [];
            foreach ($props as $key => $value) {
                $where[] = $key . ' = :' . $key;
                $prop_values[':'] = $value;
            }
            $where = (!empty($where)) ? 'WHERE ' . implode(' AND ', $where) : '';
            $sql = "SELECT * FROM user $where LIMIT 1";
            $sth = \gun_machine\classes\queryBuilderClass::prepare($sql);
            $sth->execute($props);
            $user = $sth->fetchObject(self::class);
        }
        return $user;
    }

    public static function updateUser($props = [],$user_id = 0){
        $sets = [];
        $params = [];
        if (!empty($props)){
            foreach ($props as $attr=>$val){
                $sets[] = $attr.'=:'.$attr;
                $params[':'.$attr] = $val;
            }
            $params[':id']=$user_id;
            $sql = "UPDATE user SET ".implode(',',$sets)." WHERE id = :id";
            $sth = \gun_machine\classes\queryBuilderClass::prepare($sql);
            if ($sth->execute($params)){
                $user = self::getUser(['id'=>$user_id]);
            }
        }
        return !empty($user) ? $user : false;
    }

    public static function newUser($props =[]){
        $attr = [
            'email'=>'',
            'pass'=>'',
            'register_token' =>md5('new_user'.time())
        ];

        $attr = array_replace_recursive($attr,$props);
        $names=[];
        $values =[];
        $params=[];
        foreach ($attr as $key=>$val){
            $names[] = $key;
            $values[] = ':'.$key;
            $params[':'.$key] = $val;
        }
        $names = implode(',',$names);
        $values = implode(',',$values);
        $sql = "INSERT INTO user ($names) VALUES ($values)";
        $sth = \gun_machine\classes\queryBuilderClass::prepare($sql);
        if ($sth->execute($params)){
            $user = self::getUser(['id'=>queryBuilderClass::getInsertId()]);
        }
        return empty($user) ? null : $user;
    }

}