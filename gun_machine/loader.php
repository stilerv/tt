<?php

include 'const.php';
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);


/**
 * require one (param is file to need require) or multiple (array)
 *
 * @param null|array|string $need
 * @param string $path
 * @param string $ext file extension
 */
function requiter($need = null,$path = '',$ext = '.php'){
//    var_dump($need,$path);
    $path = empty($path) || !is_string($path) ? '' : (string)$path;
    if (!empty($need)){
        if (is_string($need)){
            $need = [$need];
        }
        if (is_object($need)){
            $need = (array)$need;
        }
        if (is_array($need)){
            foreach ($need as $sub_dir => $file){
                if (is_string($file) && file_exists(__DIR__.'/'.$path.'/'.$file.$ext)){
                    require __DIR__.'/'.$path.'/'.$file.$ext;
                }
                if (is_array($file) && !empty($file)){
                    requiter($file,$path.'/'.$sub_dir);
                }
            }
        }
    }
}

$config = file_exists(APP_PATH.'/'.CONFIG_FILE) ? include APP_PATH.'/'.CONFIG_FILE : [];
$local_config = file_exists(APP_PATH.'/'.LOCAL_CONFIG_FILE) ? include APP_PATH.'/'.LOCAL_CONFIG_FILE : [];
$config = array_replace_recursive($config,$local_config);

try {
    if (empty($config)){
        throw new Exception("config file not detected");
    }
} catch (Exception $e) {
    echo $e->getMessage();
    die();
}

if (!empty($config['require'])) {
    requiter($config['require']);
}
TT::makeShot();
//TT::makeInfo();

