<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 13.09.2018
 * Time: 10:35
 */

define('ROOT_PATH',$_SERVER['DOCUMENT_ROOT']);
define('APP_PATH', __DIR__);

define('CONFIG_FILE','config.php');
define('LOCAL_CONFIG_FILE','local.config.php');

define('RESET_PASSWORD_TOKEN_LIVE_TIME',3600);
define('REGISTER_ADMIN_SECRET_KEY','Xl7dzlYPVvor2wK7gHNP');

$sub_cut = str_replace('index.php','',$_SERVER['PHP_SELF']);
define('SUBCAT_LEVEL',$sub_cut);