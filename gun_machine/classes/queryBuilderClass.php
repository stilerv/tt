<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 15.09.2018
 * Time: 11:17
 */

namespace gun_machine\classes;


/**
 * Class queryBuilderClass
 * @package gun_machine\classes
 */
class queryBuilderClass
{

    /**
     * @param string $sql
     * @return bool|\PDOStatement
     */
    public static function prepare($sql){
        global /** @var dbClass $db */
        $db;

        $sth = $db->pdo->prepare($sql);
        return $sth;
    }

    /**
     * @param string $key
     * @return string
     */
    public static function getInsertId($key = 'id'){
        global /** @var dbClass $db */
        $db;

        return $db->pdo->lastInsertId($key);
    }
}