<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 13.09.2018
 * Time: 9:29
 */

namespace gun_machine\classes;


class dbClass
{
    /**
     * @var string
     */
    private $db_name;
    /**
     * @var string
     */
    private $db_host;
    /**
     * @var string
     */
    private $db_user;
    /**
     * @var string
     */
    private $db_password = '';
    /**
     * @var \PDO
     */
    public $pdo;

    /**
     * @return string
     */
    public function getDbName()
    {
        return $this->db_name;
    }

    /**
     * @param string $db_name
     */
    public function setDbName($db_name)
    {
        $this->db_name = trim($db_name);
    }

    /**
     * @return string
     */
    public function getDbHost()
    {
        return $this->db_host;
    }

    /**
     * @param string $db_host
     */
    public function setDbHost($db_host)
    {
        $this->db_host = trim($db_host);
    }

    /**
     * @return string
     */
    public function getDbUser()
    {
        return $this->db_user;
    }

    /**
     * @param string $db_user
     */
    public function setDbUser($db_user)
    {
        $this->db_user = trim($db_user);
    }

    /**
     * @return string
     */
    public function getDbPassword()
    {
        return $this->db_password;
    }

    /**
     * @param string $db_password
     */
    public function setDbPassword($db_password)
    {
        $this->db_password = trim($db_password);
    }

    public function __construct($params)
    {
        if (is_array($params)) {
            if (isset($params['name']) && trim((string)$params['name']) != '') {
                $this->setDbName($params['name']);
            }
            if (isset($params['host']) && trim((string)$params['host']) != '') {
                $this->setDbHost($params['host']);
            }
            if (isset($params['user']) && trim((string)$params['user']) != '') {
                $this->setDbUser($params['user']);
            }
            if (isset($params['password'])) {
                $this->setDbPassword($params['password']);
            }
            $this->connect();
        }

        if (is_object($params) && get_class($params) == 'PDO') {
            $this->pdo = $params;
        }
    }

    public function connect()
    {
        try {
            $this->pdo = new \PDO('mysql:host=' . $this->getDbHost() . ';dbname=' . $this->getDbName(),
                $this->getDbUser(), $this->getDbPassword(),
                [\PDO::ATTR_AUTOCOMMIT => true]);
        } catch (\PDOException $PDOException) {

        }
    }
}