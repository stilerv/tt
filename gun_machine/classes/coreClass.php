<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 13.09.2018
 * Time: 10:30
 */

namespace gun_machine\classes;


//use gun_machine\builds\pageController;

use gun_machine\builds\userModel;

class coreClass
{
    private $config = [];
    public $db;
    public $route;
    public $user;
    public $controller;

    public function __construct()
    {
        global $db,$config;

        session_start();

        if (!is_object($db) && get_class($db) != 'dbClass'){
            $db = new dbClass($config['db']);
        }

        $this->config = $config;
        $this->db = $db;
        if (!empty($_SESSION['user_id'])){
            $this->user = userModel::getUser(['id'=>(int)$_SESSION['user_id'],'status'=>userModel::USER_STATUS_CONFIRMED]);
            $_SESSION['user'] = $this->user;
        }else{
            unset($_SESSION['user']);
        }
        return $this;
    }

    public function makeGun(){
        $this->route = new routeClass();
        return $this;
    }

    public function getShot(){
        $class_name = 'gun_machine\\builds\\'.$this->route->controller . 'Controller';
        $class_action = $this->route->action.'Action';
        ob_start();
        if (method_exists($class_name,$class_action)){
            $controller = new $class_name($this->route,$this->db);
            $controller->$class_action();
            $this->controller = $controller;
        }else{
            controllerClass::partialRender('404');
        }
        controllerClass::partialRender('site',[
            'content'=>ob_get_clean(),
            'core'=>$this]);
    }

    public function getInfo(){
        phpinfo();
    }
}