<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 13.09.2018
 * Time: 9:48
 */

namespace gun_machine\classes;


class controllerClass
{
    public $route;
    public $db;
    public $template = '';

    /**
     * controllerClass constructor.
     * @param $route routeClass
     */
    public function __construct($route,$db)
    {
        $this->route = $route;
        $this->db = $db;
    }

    public function render($params = []){
        extract($params);
        $file = APP_PATH.'/templates/'.$this->template.'.php';
        if (file_exists($file))
        include $file;
    }
    public static function partialRender($template,$params = []){
        extract($params);
        $file = APP_PATH.'/templates/'.$template.'.php';
        if (file_exists($file))
            include $file;
    }

    public static function getPartialRender($template,$params = [])
    {
        extract($params);
        $file = APP_PATH.'/templates/'.$template.'.php';
        ob_start();
        if (file_exists($file)){
            include $file;
        }
        return ob_get_clean();
    }

    public function redirect($to = '/'){
        if ($to[0] ='/') {
            $to = substr($to,1);
        }
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: ".SUBCAT_LEVEL.$to);
        exit();
    }
}