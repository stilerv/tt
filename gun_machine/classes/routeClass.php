<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 13.09.2018
 * Time: 9:45
 */

namespace gun_machine\classes;


class routeClass
{
    public $controller = 'page';
    public $action = 'main';
    public $url_params;

    public function __construct()
    {
        $url_arr = parse_url($_SERVER['REQUEST_URI']);
        if (stripos($url_arr['path'],SUBCAT_LEVEL,0) == 0){
            $req_uri = substr($url_arr['path'],strlen(SUBCAT_LEVEL)-1);
        }
        if (strlen($req_uri)>0 && $req_uri[0] == '/'){
            $req_uri = substr($req_uri,1);
        }
        $parts_url = explode('/',$req_uri);
//        array_shift($parts_url);
        if (!empty($parts_url[0]))
            $this->controller = array_shift($parts_url);
        if (!empty($parts_url[0]))
            $this->action = array_shift($parts_url);
        if (!empty($parts_url))
            $this->url_params = $parts_url;
    }
}