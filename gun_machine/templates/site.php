<?php
/**
 * @var \gun_machine\classes\coreClass $core
 */
use \gun_machine\classes\controllerClass;
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?= controllerClass::getPartialRender('head') ?>
    </head>
    <body>
        <div id="particles"></div>
        <div class="content container">
            <?= controllerClass::getPartialRender('header',['route'=>$core->route]) ?>
            <?= /** @var string $content */ $content ?>
        </div>
        <?= controllerClass::getPartialRender('footer') ?>
    </body>
</html>
