<div class="alert alert-danger" role="alert">
    Activation of account aborted. It possible that account is already activate or activation token not found.
</div>