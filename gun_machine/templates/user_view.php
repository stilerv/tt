<?php
/** @var gun_machine\classes\coreClass $core */

/** @var gun_machine\builds\userModel $user */

use gun_machine\builds\userModel;

?>
<div class="row">
    <div class="col-12">
        <?php
        foreach (userModel::viewedFields() as $atr) {
            $val = is_null($user->$atr) ? '' : $user->$atr;
            echo '<div class="input-group mt-3"><div class="input-group-prepend">
<span class="input-group-text bg-info text-white border border-info field-label">' . $atr . '</span></div>
<input type="text" class="form-control" value="' . $val . '" readonly disabled></div>';
        }
        ?>
    </div>
</div>
