<?php /**
 * @var gun_machine\classes\routeClass $route
 * @var gun_machine\builds\userModel $user
 */
$name = trim((string)$user->first_name.' '.(string)$user->last_name);
$u_name = !empty($name) ? $name : $user->email;
?>
<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle text-white" id="navUserName" data-toggle="dropdown" role="button" href="#" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user"></i><?= $u_name ?></a>
    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navUserName">
        <a class="dropdown-item <?= $route->action == 'view' ? 'active' :'' ?>" href="<?= SUBCAT_LEVEL ?>user/view" ><i class="far fa-address-card"></i>View profile</a>
        <a class="dropdown-item <?= $route->action == 'edit' ? 'active' :'' ?>" href="<?= SUBCAT_LEVEL ?>user/edit" ><i class="fas fa-user-edit"></i>Edit profile</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="<?= SUBCAT_LEVEL ?>user/logout" ><i class="fas fa-sign-out-alt"></i>Logout</a>
    </div>
</li>