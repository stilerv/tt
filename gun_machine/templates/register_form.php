<div class="login_form">
    <h1>Registration form</h1>
    <hr class="my-4">
    <form action="<?= SUBCAT_LEVEL ?>user/register" name="register" method="post">
        <div class="form-group">
            <label for="email">Email address</label>
            <input type="email" class="form-control <?= !empty($email_status) ? 'is-invalid' :'' ?>" value="<?= $_POST['email'] ?>" name="email" id="email" placeholder="Enter email">
            <?php
                if (isset($email_status)){
                    echo '<div class="invalid-feedback">'.$email_status.'</div>';
                }
            ?>
        </div>
        <div class="form-group">
            <label for="pass">Password</label>
            <input type="password" class="form-control <?= !empty($pass_status) ? 'is-invalid' :'' ?>" name="pass" id="pass" placeholder="Password">
            <?php
            if (isset($pass_status)){
                echo '<div class="invalid-feedback">'.$pass_status.'</div>';
            }
            ?>
        </div>
        <div class="form-group">
            <label for="confirm_pass">Confirm password</label>
            <input type="password" class="form-control <?= !empty($pass_status) ? 'is-invalid' :'' ?>" name="confirm_pass" id="confirm_pass" placeholder="Confirm password">
        </div>
        <button type="submit" name="register" class="btn btn-primary">Register</button>
    </form>
</div>