<div class="login_form">
    <h1>Login form</h1>
    <hr class="my-4">
    <form action="<?= SUBCAT_LEVEL ?>user/login" name="login" method="post">
        <div class="form-group">
            <label for="email">Email address</label>
            <input type="email" class="form-control" name="email" id="email" placeholder="Enter email">
        </div>
        <div class="form-group">
            <label for="pass">Password</label>
            <input type="password" class="form-control" name="pass" id="pass" placeholder="Password">
        </div>
        <button type="submit" name="login" class="btn btn-primary">Login</button>
    </form>
</div>