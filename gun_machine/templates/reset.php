<div class="login_form">
    <h1>Reset form</h1>
    <hr class="my-4">
    <form action="<?= SUBCAT_LEVEL ?>user/reset" name="rest" method="post">
        <div class="form-group">
            <label for="email">Email address</label>
            <input type="email" class="form-control <?= !empty($email_status) ? 'is-invalid' :'' ?>" value="<?= $_POST['email'] ?>" name="email" id="email" placeholder="Enter email">
            <?php
            if (isset($email_status)){
                echo '<div class="invalid-feedback">'.$email_status.'</div>';
            }
            ?>
        </div>
        <button type="submit" name="reset" class="btn btn-primary">Reset</button>
    </form>
</div>