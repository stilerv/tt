<?php /** @var gun_machine\classes\routeClass $route */?>
<li class="nav-item">
    <a class="nav-link text-white <?= $route->action == 'login' ? 'active' :'' ?>" href="<?= SUBCAT_LEVEL ?>user/login"><i class="fas fa-sign-in-alt"></i>Login</a>
</li>
<li class="nav-item">
    <a class="nav-link text-white <?= $route->action == 'register' ? 'active' :'' ?>" href="<?= SUBCAT_LEVEL ?>user/register">Register</a>
</li>
<li class="nav-item">
    <a class="nav-link text-white <?= $route->action == 'reset' ? 'active' :'' ?>" href="<?= SUBCAT_LEVEL ?>user/reset">Reset</a>
</li>