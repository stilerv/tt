<?php
/** @var gun_machine\classes\coreClass $core */

/** @var gun_machine\builds\userModel $user */

use gun_machine\builds\userModel;

?>
<div class="row">
    <div class="col-12">
        <form action="<?= SUBCAT_LEVEL ?>user/edit" name="login" method="post">
            <?php
            foreach (userModel::editableFields() as $atr) {
                $val = is_null($user->$atr) ? '' : $user->$atr;
                echo '<div class="input-group mt-3"><div class="input-group-prepend">
<span class="input-group-text bg-info text-white border border-info field-label">' . $atr . '</span></div>
<input type="text" name="' . $atr . '" class="form-control" value="' . $val . '"></div>';
            }
            ?>
            <hr class="my-1">
            <small class="form-text text-muted">If you want change password, input it into next fields</small>
            <div class="input-group mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text bg-info text-white border border-info field-label">password</span>
                </div>
                <input title="password" type="password" name="pass" class="form-control" value="">
            </div>
            <div class="input-group mt-3 mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text bg-info text-white border border-info field-label">confirm password</span>
                </div>
                <input type="password" title="confirm password" name="confirm_pass" class="form-control" value="">
            </div>
            <button type="submit" name="edit" class="btn btn-primary btn-block">Apply</button>
        </form>
    </div>
</div>
