<?php
/** @var \gun_machine\classes\routeClass $route */

$user = \gun_machine\builds\userModel::isLogged();

?>
<div class="header row">
    <ul class="nav justify-content-end nav-pills">
        <?= \gun_machine\classes\controllerClass::getPartialRender(empty($user) ? 'login' : 'logout', ['user' => $user,'route'=>$route]); ?>
    </ul>
</div>