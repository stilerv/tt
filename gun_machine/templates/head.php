    <title>TT</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?= SUBCAT_LEVEL ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= SUBCAT_LEVEL ?>assets/css/fontawesome_all.min.css">
    <link rel="stylesheet" href="<?= SUBCAT_LEVEL ?>assets/css/smile_face.css">
    <link rel="stylesheet" href="<?= SUBCAT_LEVEL ?>assets/css/main.css">

    <script src="<?= SUBCAT_LEVEL ?>assets/js/jquery.min.js"></script>
    <script src="<?= SUBCAT_LEVEL ?>assets/js/bootstrap.bundle.js"></script>
    <script src="<?= SUBCAT_LEVEL ?>assets/js/fontawesome_all.min.js"></script>
    <script src="<?= SUBCAT_LEVEL ?>assets/js/particleground.min.js"></script>
    <script src="<?= SUBCAT_LEVEL ?>assets/js/tween_max.min.js"></script>
    <script src="<?= SUBCAT_LEVEL ?>assets/js/MorphSVGPlugin.min.js"></script>
    <script src="<?= SUBCAT_LEVEL ?>assets/js/findShapeIndex.js"></script>