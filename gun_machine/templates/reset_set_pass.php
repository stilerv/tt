<div class="login_form">
    <form action="<?= SUBCAT_LEVEL ?>user/reset" name="rest" method="post">
        <div class="form-group">
            <label for="pass">Password</label>
            <input type="password" class="form-control <?= !empty($pass_status) ? 'is-invalid' :'' ?>" name="pass" id="pass" placeholder="Password">
            <?php
            if (isset($pass_status)){
                echo '<div class="invalid-feedback">'.$pass_status.'</div>';
            }
            ?>
        </div>
        <div class="form-group">
            <label for="confirm_pass">Confirm password</label>
            <input type="password" class="form-control <?= !empty($pass_status) ? 'is-invalid' :'' ?>" name="confirm_pass" id="confirm_pass" placeholder="Confirm password">
        </div>
        <input type="hidden" name="reset_token" value="<?= empty($token) ? '' :$token ?>">
        <button type="submit" name="submit_reset" class="btn btn-primary">Change</button>
    </form>
</div>