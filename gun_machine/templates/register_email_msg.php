<?php
/** @var \gun_machine\builds\userModel $user */
    $url = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
    $url .= $_SERVER['SERVER_NAME'].SUBCAT_LEVEL.'user/activate/'.$user->register_token;
?>
<html>
<head>
    <title>Your registration!</title>
</head>
<body>
For confirm registration go to <a href="<?= $url ?>" target="_blank">this</a> page.
</body>
</html>