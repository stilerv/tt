<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 13.09.2018
 * Time: 9:19
 */
return [
    'db'=>[
        'name' => 'test_task',
        'host' => 'localhost',
        'user' => 'testdev',
        'password' => 'dev00155'
    ],
    'require'=>[
        'classes' => [
            'coreClass',
            'dbClass',
            'routeClass',
            'controllerClass',
            'queryBuilderClass'
        ],
        'builds' =>[
            'userController',
            'pageController',
            'userModel'
        ]
        ,
        'tt',
    ]
];