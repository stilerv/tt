<?php

use gun_machine\classes\coreClass;

class TT extends coreClass
{

    public function __construct()
    {
        parent::__construct();
    }

    public static function makeShot()
    {
        (new parent())->makeGun()->getShot();
    }

    public static function makeInfo(){
        (new parent())->getInfo();
    }
}